from django.contrib import admin

# when registering model with admin, import it
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")
